//
//  HourCell.swift
//
//  Created by Nick Watson on 19/03/19.
//  Copyright © 2019 Nick Watson. All rights reserved.
//

import UIKit

class HourCell: UICollectionViewCell {
    
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
}
