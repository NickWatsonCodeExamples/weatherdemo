//
//  CustomLoadingIndicator.swift
//
//  Created by Nick Watson on 19/03/19.
//  Copyright © 2019 Nick Watson. All rights reserved.
//

import Foundation
import UIKit

class CustomLoadingIndicator {
    
    // this class is a singleton in order for
    // the same views to be added and removed each time
    //
    static let shared = CustomLoadingIndicator()
    
    var loadingView: UIView = UIView()
    var loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func showLoadingIndicator(view: UIView) {
        view.isUserInteractionEnabled = false
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = view.center
        loadingView.backgroundColor = UIColor.clear
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        loadingIndicator.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        loadingIndicator.style = .gray
        loadingIndicator.center = CGPoint(x: loadingView.frame.width / 2, y: loadingView.frame.height / 2)
        loadingIndicator.hidesWhenStopped = true
        
        DispatchQueue.main.async {
            self.loadingView.addSubview(self.loadingIndicator)
            view.addSubview(self.loadingView)
            self.loadingIndicator.startAnimating()
        }
    }
    
    func hideLoadingIndicator(view: UIView) {
        if loadingView.isDescendant(of: view) {
            DispatchQueue.main.async {
                view.isUserInteractionEnabled = true
                self.loadingIndicator.stopAnimating()
                self.loadingView.removeFromSuperview()
            }
        }
    }
}
