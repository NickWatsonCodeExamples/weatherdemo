//
//  HourWeather.swift
//
//  Created by Nick Watson on 19/03/19.
//  Copyright © 2019 Nick Watson. All rights reserved.
//

import Foundation
import SwiftyJSON

struct HourWeather {
    
    var temperature: Double
    let icon: String
    let time: Double
    
    init(json: JSON) {
        self.temperature = json["temperature"].doubleValue
        self.icon = json["icon"].stringValue
        self.time = json["time"].doubleValue
        
    }
}
