//
//  Weather.swift
//
//  Created by Nick Watson on 19/03/19.
//  Copyright © 2019 Nick Watson. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Weather {
    var currentTemp: Double?
    
    let lowTemp: Double
    let highTemp: Double
    let summary: String
    let icon: String
    let time: Double
    
    init(json: JSON) {
        self.lowTemp = json["temperatureLow"].doubleValue
        self.highTemp = json["temperatureHigh"].doubleValue
        self.summary = json["summary"].stringValue
        self.icon = json["icon"].stringValue
        self.time = json["time"].doubleValue
    }
    
    init(fullJson: JSON) {
        self.currentTemp = fullJson["currently"]["temperature"].doubleValue
        self.lowTemp = fullJson["daily"]["data"][0]["temperatureLow"].doubleValue
        self.highTemp = fullJson["daily"]["data"][0]["temperatureHigh"].doubleValue
        self.icon = fullJson["currently"]["icon"].stringValue
        self.time = fullJson["currently"]["time"].doubleValue
        self.summary = fullJson["hourly"]["summary"].stringValue
    }
    
    func getFormattedTime() -> String? {
        // Since time is a Double it has a trailing .0 this causes the HTTP request to fail
        // reformat the number to remove the fraction digits
        //
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 0
        return formatter.string(from: NSNumber(value: self.time))
    }
}
