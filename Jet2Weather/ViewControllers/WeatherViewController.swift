//
//  WeatherViewController.swift
//
//  Created by Nick Watson on 19/03/19.
//  Copyright © 2019 NICK WATSON. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController, CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var currentTempLbl: UILabel!
    @IBOutlet weak var summaryLbl: UILabel!
    @IBOutlet weak var currentSummaryLbl: UILabel!
    @IBOutlet weak var todayHighLbl: UILabel!
    @IBOutlet weak var todayLowLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var locationLbl: UILabel!
    
    var lastPlace: CLPlacemark?
    var lastTimestamp: Int64?
    
    var weatherArray = [Weather]()
    var hourWeather = [HourWeather]()
    
    var selectedIndex: Int = -1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        if CoreLocationWrapper.shared.canAccessLocation() {
            getPlace()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Refresh notification
        //
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        CoreLocationWrapper.shared.locationManager.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.layer.borderWidth = 1.5
        collectionView.layer.cornerRadius = 10
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        if let flow = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let width = UIScreen.main.bounds.width
            let height = collectionView.bounds.height
            flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            flow.minimumInteritemSpacing = 0
            flow.minimumLineSpacing = 0
            flow.itemSize = CGSize(width: width / 5, height: height)
        }
        
        // SearchBar text color
        //
        
        collectionView.layer.borderColor = UIColor.black.cgColor
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40
        tableView.separatorStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Ask user for permission
        //
        if !CoreLocationWrapper.shared.canAccessLocation() {
            if CoreLocationWrapper.shared.authStatus != .denied {
                CoreLocationWrapper.shared.locationManager.requestWhenInUseAuthorization()
            } else {
                showAlert(title: "Location Accees Requested",
                          message: "The location permission was not authorized. Please enable it in Settings to continue.")
            }
        }
    }
    
    // Marker: CLLocationManagerDelegate
    //
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        CoreLocationWrapper.shared.authStatus = status
        if status == .denied {
            showAlert(title: "Location Accees Requested",
                      message: "The location permission was not authorized. Please enable it in Settings to continue.")
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            // Get the users location and then the weather
            //
            getPlace()
        }
    }
    
    // Marker: CollectionView Delegate
    //
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourWeather.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hourlyCell", for: indexPath) as! HourCell
        
        if let temp = hourWeather[indexPath.row].temperature.stringFixFormat {
            cell.tempLbl.text = "\(temp)°"
        }
        //if let precipPercent = hourlyWeather[indexPath.row].precipProbability.percentString {
        //    cell.precipLabel.text = precipPercent
        //}
        cell.hourLbl.text = DarkSkyAPI.timestampToHour(seconds: hourWeather[indexPath.row].time)
        cell.imageView.image = DarkSkyAPI.convertToIcon(iconName: hourWeather[indexPath.row].icon)
        return cell
    }
    
    // Marker: TableView Delegate
    //
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if weatherArray.isEmpty {
            return 0
        } else {
            // One extra row for the weekly summary
            //
            return weatherArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            // Show the weekly summary cell
            //
            let cell = tableView.dequeueReusableCell(withIdentifier: "weeklySummaryCell", for: indexPath) as! WeekSumCell
            
            if let summary = DarkSkyAPI.shared.getWeekSummary() {
                cell.summaryLbl.text = summary
                cell.selectionStyle = .none
            } else {
                cell.isHidden = true
            }
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
            // Skip index 0 which is the current days weather since it is already displayed
            //
            let index = indexPath.row
            cell.weatherImageView.image = DarkSkyAPI.convertToIcon(iconName: weatherArray[index].icon)
            cell.dayLbl.text = DarkSkyAPI.timestampToDayName(seconds: weatherArray[index].time)
            cell.highLbl.text = weatherArray[index].highTemp.stringFixFormat! + "°"
            cell.lowLbl.text = weatherArray[index].lowTemp.stringFixFormat! + "°"
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func getPlace() {
        CoreLocationWrapper.shared.getPlace(completion: { placemark, error in
            if let err = error {
                self.showAlert(title: "Error", message: err.localizedDescription)
            } else if let p = placemark {
                if self.refreshWeather(place: p) {
                    // Update the timestamp everytime the weather is refreshed
                    //
                    self.lastTimestamp = Date().millisSinceEpoch
                    CustomLoadingIndicator.shared.showLoadingIndicator(view: self.view)
                    self.parsePlacemark(placemark: p)
                }
            }
        })
    }
    
    func refreshWeather(place: CLPlacemark) -> Bool {
        if self.lastPlace == nil {
            // Save the last place we got the weather for
            //
            self.lastPlace = place
            self.lastTimestamp = Date().millisSinceEpoch
            return true
        } else if fifteenMinutesSinceLastRequest() {
            return true
        } else {
            return false
        }
    }
    
    func fifteenMinutesSinceLastRequest() -> Bool {
        // If we don't have a previous timestamp then just refresh
        //
        guard let oldMilliseconds = lastTimestamp else { return true }
        // Fifteen minutes in milliseconds
        //
        let minTimeDifference = (1000 * 60 * 15)
        if Date().millisSinceEpoch - oldMilliseconds > minTimeDifference {
            return true
        } else {
            return false
        }
    }
    
    func isNewCity(placemark: CLPlacemark) -> Bool {
        guard let newCity = placemark.locality, let oldCity = lastPlace?.locality else { return false }
        if newCity != oldCity {
            return true
        } else {
            return false
        }
    }
    
    func parsePlacemark(placemark: CLPlacemark) {
        guard let location = placemark.location else { return }
        getWeather(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        setLocationLabel(placemark: placemark)
    }
    
    func setLocationLabel(placemark: CLPlacemark) {
        let city = placemark.locality
        let state = placemark.administrativeArea
        let country = placemark.country
        
        if city != nil && state != nil {
            locationLbl.text = "\(city!), \(state!)"
        } else if city != nil && country != nil {
            locationLbl.text = "\(city!), \(country!)"
        } else if state != nil && country != nil {
            locationLbl.text = "\(state!), \(country!)"
        } else if city != nil {
            locationLbl.text = "\(city!)"
        } else if state != nil {
            locationLbl.text = "\(state!)"
        } else if country != nil {
            locationLbl.text = "\(country!)"
        } else {
            // Stranger Things Easter Egg
            //
            locationLbl.text = "Hawkins, IN"
        }
    }
    
    func getWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        DarkSkyAPI.shared.getForecast(lat: latitude, long: longitude, completionHandler: { weatherArray, hourlyArray, error in
            
            if error != nil {
                self.showAlert(title: "Error", message: error!.localizedDescription)
            }
            
            if let weather = weatherArray {
                self.weatherArray = weather
                self.tableView.reloadData()
                self.displayCurrentConditions()
            }
            if let hourly = hourlyArray {
                self.hourWeather = hourly
                self.collectionView.resetScrollPositionTop()
                self.collectionView.reloadData()
            } else {
                print("hourlyWeather = nil")
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                CustomLoadingIndicator.shared.hideLoadingIndicator(view: self.view)
            }
        })
    }
    
    func displayCurrentConditions() {
        if !weatherArray.isEmpty {
            let attribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17)]
            
            if let currentTemp = weatherArray[0].currentTemp?.stringFixFormat {
                currentTempLbl.text = "\(currentTemp)°"
            }
            
            if let high = weatherArray[0].highTemp.stringFixFormat {
                let attributedHigh = NSMutableAttributedString(string:  "Today's High  ", attributes: attribute)
                let attributedHighTemp = NSMutableAttributedString(string: "\(high)°")
                attributedHigh.append(attributedHighTemp)

                todayHighLbl.attributedText = attributedHigh
            }
            if let lo = weatherArray[0].lowTemp.stringFixFormat {
                let attributedLow = NSMutableAttributedString(string: "Today's Low   ", attributes: attribute)
                let attributedLowTemp = NSMutableAttributedString(string: "\(lo)°")
                attributedLow.append(attributedLowTemp)
                
                todayLowLbl.attributedText = attributedLow
            }
            currentSummaryLbl.text = weatherArray[0].summary
        }   
    }
    
    // Marker: SearchBar Delegate
    //
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        CustomLoadingIndicator.shared.showLoadingIndicator(view: self.view)
        searchBar.resignFirstResponder()
        guard let text = searchBar.text else { return }
        searchBar.text = ""
        CoreLocationWrapper.shared.searchFor(text: text, completion: { placemark, error in
            if let err = error {
                CustomLoadingIndicator.shared.hideLoadingIndicator(view: self.view)
                self.showAlert(title: "Error", message: err.localizedDescription)
            }
            if let p = placemark {
                self.parsePlacemark(placemark: p)
            }
        })
    }
    
    @objc func applicationWillEnterForeground() {
        getPlace()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension UIScrollView {
    
    func resetScrollPositionTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}

extension Double {
    
    var stringFixFormat: String? {
        // Rounds a Double and removes the trailing zeros from the string
        //
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 0
        let formatedString = formatter.string(from: NSNumber(value: self))
        return formatedString
    }
}

extension Date {
    // Get milliseconds since epoch
    //
    var millisSinceEpoch: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
}



