//
//  LocationWrapper.swift
//
//  Created by Nick Watson on 19/03/19.
//  Copyright © 2019 Nick Watson. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class CoreLocationWrapper {
    
    static let shared = CoreLocationWrapper()
    let locationManager = CLLocationManager()
    let clGeocoder = CLGeocoder()
    var currentLocation: CLLocation?
    var authStatus = CLLocationManager.authorizationStatus()
   
    
    func canAccessLocation() -> Bool {
        switch authStatus {
            
        case .authorizedAlways:
            return true
        case .authorizedWhenInUse:
            return true
        case .denied:
            return false
        case .restricted:
            return false
        case .notDetermined:
           return false
        @unknown default:
            return false
        }
    }
    
    func requestAccess() {
        if authStatus == .denied {
            // We cannot ask again so we just want to alert
            //
            if let topVC = UIApplication.topViewController() {
                topVC.showAlert(title: "Location Accees Requested",
                                message: "The location permission was not authorized. Please enable it in Settings to continue.")
            }
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
    }
    
    func getPlace(completion: @escaping(_ placemark: CLPlacemark?, _ error: Error?) ->()) {
        if self.authStatus == .authorizedWhenInUse || self.authStatus == .authorizedAlways {
            if let location = locationManager.location {
                self.currentLocation = location
                clGeocoder.reverseGeocodeLocation(location) { placemarks, error in
                    if let err = error {
                        completion(nil, err)
                    }
                    if let places = placemarks {
                        let placemarkArray = places as [CLPlacemark]
                        if !placemarkArray.isEmpty {
                            completion(placemarkArray[0], nil)
                        }
                    }
                }
            }
            else if let location = self.currentLocation {
                clGeocoder.reverseGeocodeLocation(location) { placemarks, error in
                    if let err = error {
                        completion(nil, err)
                    }
                    if let places = placemarks {
                        let placemarkArray = places as [CLPlacemark]
                        if !placemarkArray.isEmpty {
                            completion(placemarkArray[0], nil)
                        }
                    }
                }
            }
            else if let location = CLLocationManager().location {
                clGeocoder.reverseGeocodeLocation(location) { placemarks, error in
                    if let err = error {
                        completion(nil, err)
                    }
                    if let places = placemarks {
                        let placemarkArray = places as [CLPlacemark]
                        if !placemarkArray.isEmpty {
                            completion(placemarkArray[0], nil)
                        }
                    }
                }
            }
        } else {
            print("Auth status failed... in CoreLocationWrapper")
        }
    }
    
    func searchFor(text: String, completion: @escaping (_ placemark: CLPlacemark?, _ error: Error?) -> ()) {
        clGeocoder.geocodeAddressString(text, completionHandler: { (placemarks, error) in
            if let err = error {
                completion(nil, err)
            }
            if let places = placemarks {
                let placemarkArray = places as [CLPlacemark]
                if !placemarkArray.isEmpty {
                    completion(placemarkArray[0], nil)
                }
            }
        })
    }
}

extension UIApplication {
    // Allows us to get the "top" or visible viewcontroller from anywhere
    //
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
