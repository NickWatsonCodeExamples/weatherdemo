//
//  DarkSkyAPI.swift
//
//  Created by Nick Watson on 19/03/19.
//  Copyright © 2019 Nick Watson. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class DarkSkyAPI {
    
    static let shared = DarkSkyAPI()
    var response: JSON?
    
    var responses = [String: JSON]()
    
    func getFutureForecast(lat: Double, long: Double, formattedTime: String, completionHandler: @escaping ([HourWeather]?, Error?) ->()) {
        let apiKey = valueForAPIKey(named:"DarkSky_KEY")
            DispatchQueue.global(qos: .utility).async {
                if self.responses["\(lat)\(long)\(formattedTime)"] == nil {
                    Alamofire.request("https://api.darksky.net/forecast/\(apiKey)/\(lat),\(long),\(formattedTime)?units=si").responseJSON(completionHandler: { (responseData) -> Void in
                        
                        if let error = responseData.result.error as? AFError {
                            let err = NSError(domain: "Future Weather Request Error", code: 0, userInfo: [NSLocalizedDescriptionKey: "\(error.localizedDescription)"])
                            completionHandler(nil, err)
                        }
                        else if responseData.result.value != nil {
                            let json = JSON(responseData.result.value!)
                            self.responses["\(lat)\(long)\(formattedTime)"] = json
                            
                            // Get hourly weather for that day
                            //
                            var hourWeatherArray = [HourWeather]()
                            for hour in json["hourly"]["data"].arrayValue {
                                let hourlyWeather = HourWeather(json: hour)
                                hourWeatherArray.append(hourlyWeather)
                            }
                            completionHandler(hourWeatherArray, nil)
                        }
                    })
                }
                else if let json = self.responses["\(lat)\(long)\(formattedTime)"] {
                    // Get hourly weather for that day
                    //
                    var hourWeatherArray = [HourWeather]()
                    for hour in json["hourly"]["data"].arrayValue {
                        let hourlyWeather = HourWeather(json: hour)
                        hourWeatherArray.append(hourlyWeather)
                    }
                    completionHandler(hourWeatherArray, nil)
                }
            }
        
    }
    
    func getForecast(lat: Double, long: Double, completionHandler: @escaping ([Weather]?, [HourWeather]?, Error?) -> ()) {
        let apiKey = valueForAPIKey(named:"DarkSky_KEY")
            DispatchQueue.global(qos: .utility).async {
                // new location or refreshed weather data so we can clear responses
                //
                self.responses.removeAll()
                Alamofire.request("https://api.darksky.net/forecast/\(apiKey)/\(lat),\(long)?units=si").responseJSON { (responseData) -> Void in
                    
                    if let error = responseData.result.error as? AFError {
                        let err = NSError(domain: "Weather Request Error", code: 0, userInfo: [NSLocalizedDescriptionKey: "\(error.localizedDescription)"])
                        completionHandler(nil, nil, err)
                    }
                        
                    else if responseData.result.value != nil {
                        var weatherArray = [Weather]()
                        let swiftyJson = JSON(responseData.result.value!)
                        
                        self.response = swiftyJson
                        
                        let currentWeather = Weather(fullJson: swiftyJson)
                        weatherArray.append(currentWeather)
                        
                        var firstDay = true
                        for day in swiftyJson["daily"]["data"].arrayValue {
                            if !firstDay {
                                let dailyWeather = Weather(json: day)
                                weatherArray.append(dailyWeather)
                            } else {
                                firstDay = false
                            }
                        }
                        
                        var hourWeatherArray = [HourWeather]()
                        for hour in swiftyJson["hourly"]["data"].arrayValue {
                            let hourlyWeather = HourWeather(json: hour)
                            hourWeatherArray.append(hourlyWeather)
                        }
                        completionHandler(weatherArray, hourWeatherArray, nil)
                    }
                }
            }
        
    }
    
    func getWeekSummary() -> String? {
        if let json = response {
            return json["daily"]["summary"].stringValue
        }
        return nil
    }
    static func convertToIcon(iconName: String) -> UIImage {
        switch iconName {
        case "clear-day":
            return #imageLiteral(resourceName: "clear-day")
        case "clear-night":
            return #imageLiteral(resourceName: "clear-night")
        case "partly-cloudy-day":
            return #imageLiteral(resourceName: "partly-cloudy-day")
        case "partly-cloudy-night":
            return #imageLiteral(resourceName: "partly-cloudy-night")
        case "cloudy":
            return #imageLiteral(resourceName: "cloudy")
        case "rain":
            return #imageLiteral(resourceName: "rain")
        case "sleet":
            return #imageLiteral(resourceName: "sleet")
        case "snow":
            return #imageLiteral(resourceName: "snow")
        case "wind":
            return #imageLiteral(resourceName: "wind")
        case "fog":
            
                return #imageLiteral(resourceName: "fog")
            
        default:
            return #imageLiteral(resourceName: "error")
        }
    }
    
    static func timestampToHour(seconds: Double) -> String {
        let date = Date(timeIntervalSince1970: seconds)
        let formatter = DateFormatter()
        // a is "pm" or "am"
        //
        formatter.dateFormat = "h a"
        let hourString = formatter.string(from: date)
        return hourString
    }
    
    static func timestampToDayName(seconds: Double) -> String {
        let date = Date(timeIntervalSince1970: seconds)
        let format = "EEEE"
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    func valueForAPIKey(named keyname:String) -> String {
        // Credit to the original source for this technique
        // http://blog.lazerwalker.com/blog/2014/05/14/handling-private-api-keys-in-open-source-ios-apps
        let filePath = Bundle.main.path(forResource: "APIKeys", ofType: "plist")
        let plist = NSDictionary(contentsOfFile:filePath!)
        let value = plist?.object(forKey: keyname) as! String
        return value
    }
}

